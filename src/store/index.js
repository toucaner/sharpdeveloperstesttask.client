import Vue from 'vue';
import Vuex from 'vuex';

import { alert } from './alert.module';
import { account } from './account.module';
import { pwaccount } from './pwaccount.module';
import { users } from './users.module';
import { transactions } from './transaction.module';

Vue.use(Vuex);

export const store = new Vuex.Store({
    modules: {
        alert,
        account,
        pwaccount,
        users,
        transactions
    }
});
