import { pwaccountService } from '../services';
import { router } from '../helpers';

const user = JSON.parse(localStorage.getItem('user'));
const state = {
        pwaccount: {},
        all: {}
    };

const actions = {
    getAccount({ commit }) {
        commit('getPwaccountRequest');

        pwaccountService.getAccount(user.id)
            .then(
                pwaccount => commit('getPwaccountSuccess', pwaccount),
                error => commit('getPwaccountFailure', error)
            );
    },
    getAllAccounts({ commit }) {
        commit('getAllPwaccountRequest');

        pwaccountService.getAllAccounts(user.id)
            .then(
                pwaccounts => commit('getAllPwaccountSuccess', pwaccounts),
                error => commit('getAllPwaccountFailure', error)
            );
    }
};

const mutations = {
    getPwaccountRequest(state) {
        state.pwaccount = { loading: true };
    },
    getPwaccountSuccess(state, pwaccount) {
        state.pwaccount = pwaccount;
    },
    getPwaccountFailure(state, error) {
        state.pwaccount = { error };
    },
    getAllPwaccountRequest(state) {
        state.all = { loading: true };
    },
    getAllPwaccountSuccess(state, pwaccounts) {
        state.all = pwaccounts;
    },
    getAllPwaccountFailure(state, error) {
        state.all = { error };
    },
};

export const pwaccount = {
    namespaced: true,
    state,
    actions,
    mutations
};