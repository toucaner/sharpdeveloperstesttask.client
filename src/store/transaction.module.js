import { transactionService } from '../services';
import { router } from '../helpers';

const pwaccount = JSON.parse(localStorage.getItem('pwaccount'));
const state = {
        transactions: {}
    };

const actions = {
    getAllTransaction({ commit }) {
        commit('getAllTransactionRequest');

        transactionService.getAllTransaction(pwaccount.id)
            .then(
                transactions => commit('getAllTransactionSuccess', transactions),
                error => commit('getAllTransactionFailure', error)
            );
    },
    addTransaction({ commit }, transaction ) {
        transactionService.addTransaction(transaction)
            .then(
                transaction => commit('getAllTransactionSuccess'),
                error => commit('getAllTransactionFailure', error)
            );
    }
};

const mutations = {
    addTransactionSuccess(state, transactions) {
        dispatch('alert/success', 'Create transaction successful', { root: true });
    },
    addTransactionFailure(state, error) {
        dispatch('alert/error', error, { root: true });
    },
    getAllTransactionRequest(state) {
        state.transactions = { loading: true };
    },
    getAllTransactionSuccess(state, transactions) {
        state.transactions = { items: transactions };
    },
    getAllTransactionFailure(state, error) {
        state.transactions = { error };
    },
};

export const transactions = {
    namespaced: true,
    state,
    actions,
    mutations
};