import config from 'config';
import { authHeader } from '../helpers';

export const transactionService = {
    getTransaction,
    getAllTransaction, 
    addTransaction
};

function getTransaction(id) {
    const requestOptions = {
        method: 'GET',
        headers: authHeader()
    };

    return fetch(`${config.apiUrl}/transactions/getbyid/${id}`, requestOptions).then(handleResponse);
}

function addTransaction(transaction) {
    const requestOptions = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(transaction)
    };

    return fetch(`${config.apiUrl}/transactions/add`, requestOptions).then(handleResponse);
}

function getAllTransaction(id) {
    const requestOptions = {
        method: 'GET',
        headers: authHeader()
    };

    return fetch(`${config.apiUrl}/transactions/getall/${id}`, requestOptions).then(handleResponse);
}

function handleResponse(response) {
    return response.text().then(text => {
        const data = text && JSON.parse(text);
        if (!response.ok) {
            if (response.status === 401) {
                location.reload(true);
            }

            const error = (data && data.message) || response.statusText;
            return Promise.reject(error);
        }

        return data;
    });
}