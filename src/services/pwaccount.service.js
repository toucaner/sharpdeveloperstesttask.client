import config from 'config';
import { authHeader } from '../helpers';

const pwaccount = JSON.parse(localStorage.getItem('pwaccount'));

export const pwaccountService = {
    getAccount,
    getAllAccounts
};

function getAccount(id) {
    const requestOptions = {
        method: 'GET',
        headers: authHeader()
    };

    return fetch(`${config.apiUrl}/accounts/getbyid/${id}`, requestOptions)
    .then(handleResponse)
    .then(pwaccount => {
        if (pwaccount) {
            localStorage.setItem('pwaccount', JSON.stringify(pwaccount));
        }

        return pwaccount;
    });
}

function getAllAccounts() {
    const requestOptions = {
        method: 'GET',
        headers: authHeader()
    };
    var id = pwaccount.id;
    return fetch(`${config.apiUrl}/accounts/getall/${id}`, requestOptions).then(handleResponse);
}

function handleResponse(response) {
    return response.text().then(text => {
        const data = text && JSON.parse(text);
        if (!response.ok) {
            if (response.status === 401) {
                location.reload(true);
            }

            const error = (data && data.message) || response.statusText;
            return Promise.reject(error);
        }

        return data;
    });
}