import Vue from 'vue';
import VeeValidate from 'vee-validate';
import TransactionTable from './components/transaction/TransactionTable'
import TransactionModal from './components/transaction/TransactionModal'
import VueMomentLib from 'vue-moment-lib';
import VueMaterial from 'vue-material'
import 'vue-material/dist/vue-material.min.css'
import SortedTablePlugin from "vue-sorted-table";

import { store } from './store';
import { router } from './helpers';
import App from './view/app/App';

Vue.use(VeeValidate);
Vue.use(VueMomentLib);
Vue.use(VueMaterial);
Vue.use(SortedTablePlugin);
Vue.component('transaction-table', TransactionTable);
Vue.component('transaction-modal', TransactionModal);

new Vue({
    router,
    store,
    render: h => h(App)
}).$mount('#app');